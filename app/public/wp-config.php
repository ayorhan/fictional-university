<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //

if(file_exists(dirname(__FILE__) . '/local.php')){
    define( 'DB_NAME', 'local' );
    define( 'DB_USER', 'root' );
    define( 'DB_PASSWORD', 'root' );
    define( 'DB_HOST', 'localhost' );
} else {
    define( 'DB_NAME', 'yigithan_university' );
    define( 'DB_USER', 'yigithan_wp889' );
    define( 'DB_PASSWORD', 'D2IEBTUt*E%m' );
    define( 'DB_HOST', 'localhost' );
}

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0B|2Ib81_!J umZ`3XUpm%Q>-+XddRcS]3 4 qP_a|e[1{6a5IgW}LPe3m(4k3Hn');
define('SECURE_AUTH_KEY',  'Qd<PT}F&UGjUm<>taQ8/%UrbS|[8GBs43OcB{e|jmY0`=-M!VuM-;r6H2-,wB@:,');
define('LOGGED_IN_KEY',    'in,[W8MH*;-M7:)$_%nNo0DhA*D*#W-S>3blmBX4lH4g!96+t7Kyj*)~-qD.3h|!');
define('NONCE_KEY',        '7om./$S#c=7&T;yTUW@2JOrfT{)-m6/3#&B,+aB~7uAWrvL,0:Pm4br#AT^c,a@(');
define('AUTH_SALT',        'z$JmhNO^oV2%)|>=!-{fWo{@[>~fX2}UTB}{k%F|QEC<z3a}r{Sq-Zx(unbar[C1');
define('SECURE_AUTH_SALT', '9i=*{6oKtrY~M$QoAXh:e9DZ`q.S%a#([B~ak}/)) +*>HhtH{Z`Y@Lwn_%^PvWt');
define('LOGGED_IN_SALT',   'WM|]`|2tvulB`5kQ [Mq?Ugi:WY|d]V$-Wz,MNlW2{A-+Q<T&h]h^,b]W@tMr4r9');
define('NONCE_SALT',       'z?Y#sR+R~vnHywe~JUy()<7Qk49?PqL{%FTb<b:1f)20fcRhj-acrmeQVegarOJ[');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
