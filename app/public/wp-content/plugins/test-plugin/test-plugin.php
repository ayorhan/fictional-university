<?php

/**
 * Plugin Name: Test Plugin Name
 * Description: Just trying out
 * Author: Kolektif Labs
 * Version: 1.0.0
 */

add_filter('the_content', 'contentEdits');

function contentEdits($content){
    $content .= '<p>Taking the test plugin on a spin.</p>';
    $content = str_replace('Lorem', '*****', $content);
    return $content;
}

add_shortcode('programCount', 'programCountFunction');

function programCountFunction(){
    return 5;
}